package at.ac.tuwien.inso.sqm.enums;

public enum SubjectType {
    MANDATORY,
    OPTIONAL,
    FREE_CHOICE
}
