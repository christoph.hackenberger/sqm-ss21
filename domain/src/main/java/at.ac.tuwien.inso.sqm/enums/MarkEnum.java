package at.ac.tuwien.inso.sqm.enums;

public enum MarkEnum {

    EXCELLENT(1),
    GOOD(2),
    SATISFACTORY(3),
    SUFFICIENT(4),
    FAILING(5);

    private int mark;

    MarkEnum(int mark) {
        this.mark = mark;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String toString() {
        return "MarkEntity" + mark;
    }

    public MarkEnum toEnumConstant(int markInt) {
        for (MarkEnum m : values()) {
            if (m.getMark() == markInt) {
                return m;
            }
        }

        return null;
    }
}
