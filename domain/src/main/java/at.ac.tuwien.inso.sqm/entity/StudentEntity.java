package at.ac.tuwien.inso.sqm.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

@Entity
public class StudentEntity extends UisUserEntity {

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StudyPlanRegistration> studyplans = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Lecture> dismissedCourses = new ArrayList<>();

    protected StudentEntity() {
    }

    public StudentEntity(String identificationNumber, String name,
                         String email) {
        this(identificationNumber, name, email, null);
    }

    public StudentEntity(String identificationNumber, String name, String email,
                         UserAccountEntity account) {
        super(identificationNumber, name, email, account);
    }

    @Override
    protected void adjustRole(UserAccountEntity accountToAdjustTo) {
        accountToAdjustTo.setRole(Rolle.STUDENT);
    }

    public List<StudyPlanRegistration> getStudyplans() {
        return studyplans;
    }

    public StudentEntity addStudyplans(
            StudyPlanRegistration... studyplansToAdd) {
        this.studyplans.addAll(asList(studyplansToAdd));
        return this;
    }

    public List<Lecture> getDismissedCourses() {
        return dismissedCourses;
    }

    public void setDismissedCourses(List<Lecture> dismissedCourses) {
        this.dismissedCourses = dismissedCourses;
    }

    public StudentEntity addDismissedCourse(
            Lecture... dismissedCourseToAdd) {
        this.dismissedCourses.addAll(asList(dismissedCourseToAdd));
        return this;
    }
}
