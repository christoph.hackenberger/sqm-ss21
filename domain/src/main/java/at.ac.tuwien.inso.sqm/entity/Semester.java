package at.ac.tuwien.inso.sqm.entity;

import at.ac.tuwien.inso.sqm.dto.SemesterDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Semester {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private int yaer;

    /**
     * WS or SS.
     */
    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private SemesterTypeEnum type;

    public Semester() {
    }

    public Semester(int yaer, SemesterTypeEnum type) {
        this.yaer = yaer;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYaer() {
        return yaer;
    }

    public void setYaer(int yaer) {
        this.yaer = yaer;
    }

    public SemesterTypeEnum getType() {
        return type;
    }

    public void setType(SemesterTypeEnum type) {
        this.type = type;
    }

    public String getLabel() {
        return getType() + " " + getYaer();
    }

    public String toString() {
        return getLabel();
    }

    public SemesterDto toDto() {
        SemesterDto dto = new SemesterDto(yaer, type);

        if (id != null) {
            dto.setId(id);
        }

        return dto;
    }
}
