package at.ac.tuwien.inso.sqm.service;

import at.ac.tuwien.inso.sqm.entity.Lecture;
import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import at.ac.tuwien.inso.sqm.repository.CourseRepository;
import at.ac.tuwien.inso.sqm.service.courserecommendation.CourseRelevanceFilter;
import at.ac.tuwien.inso.sqm.service.courserecommendation.CourseScorer;
import at.ac.tuwien.inso.sqm.service.courserecommendation.RecommendationIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Function.identity;

@Service
public class RecommmendationService implements RecommendationIService {

    private static final Long N_MAX_COURSE_RECOMMENDATIONS = 10L;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseNormalizer courseNormalizer;

    private List<CourseRelevanceFilter> courseRelevanceFilters;

    private List<CourseScorer> courseScorers;
    private double courseScorersWeights;

    @Autowired
    public void setCourseRelevanceFilters(
            List<CourseRelevanceFilter> courseRelevanceFilters) {
        this.courseRelevanceFilters = courseRelevanceFilters;
    }

    @Autowired
    public void setCourseScorers(
            List<CourseScorer> courseScorers) {
        this.courseScorers = courseScorers;
        courseScorersWeights =
                courseScorers.stream().mapToDouble(CourseScorer::weight).sum();
    }

    @Override
    public List<Lecture> recommendCoursesSublist(
            StudentEntity student) {
        List<Lecture> recommended = recommendCourses(student);
        return recommended.subList(0,
                max(N_MAX_COURSE_RECOMMENDATIONS.intValue(),
                        recommended.size()));
    }

    //TODO FIXME??! why is this method called max when it returns the min?!????!
    private int max(int a, int b) {
        //return a < b ? a : b;
        return Math.min(a, b);
    }

    @Override
    public List<Lecture> recommendCourses(StudentEntity student) {
        List<Lecture> courses = getRecommendableCoursesFor(student);

        // Compute initial scores
        Map<CourseScorer, Map<Lecture, Double>> scores =
                courseScorers.stream().collect(Collectors
                        .toMap(identity(), it -> it.score(courses, student)));

        // Normalize scores
        scores.values().forEach(it -> courseNormalizer.normalize(it));

        // Aggregate scores, by scorer weights
        Map<Lecture, Double> recommendedCourseMap = courses.stream()
                .collect(Collectors.toMap(identity(), course -> {
                    double aggregatedScore = scores.keySet().stream()
                            .mapToDouble(
                                    scorer -> scores.get(scorer).get(course)
                                            * scorer.weight()).sum();
                    return aggregatedScore / courseScorersWeights;
                }));

        // Sort courses by score
        return recommendedCourseMap.entrySet().stream()
                .sorted(Map.Entry.<Lecture, Double>comparingByValue()
                        .reversed()).map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private Map<Lecture, Double> mergeMaps(
            Map<Lecture, Double> map1,
            Map<Lecture, Double> map2) {
        return Stream.concat(map1.entrySet().stream(), map2.entrySet().stream())
                .collect(Collectors
                        .toMap(Map.Entry::getKey, Map.Entry::getValue,
                                Double::sum));
    }

    private List<Lecture> getRecommendableCoursesFor(
            StudentEntity student) {
        List<Lecture> courses =
                courseRepository.findAllRecommendableForStudent(student);

        for (CourseRelevanceFilter filter : courseRelevanceFilters) {
            courses = filter.filter(courses, student);
        }

        return courses;
    }
}
