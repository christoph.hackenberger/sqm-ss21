package at.ac.tuwien.inso.sqm.repository;

import at.ac.tuwien.inso.sqm.entity.Lecture;
import at.ac.tuwien.inso.sqm.entity.Semester;
import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import at.ac.tuwien.inso.sqm.entity.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository
        extends CrudRepository<Lecture, Long> {

    Page<Lecture> findAllBySemesterAndSubjectNameLikeIgnoreCase(
            Semester semester, String name, Pageable pageable);

    List<Lecture> findAllBySemesterAndSubject(Semester semester,
                                              Subject subject);

    List<Lecture> findAllBySubject(Subject subject);

    @Query("select c "
            + "from Lecture c " + "where c.semester = ("
            + "select s " + "   from Semester s " + "   where s.id = ( "
            + "select max(s1.id) " + "       from Semester s1 "
            + ")" + "   ) " + "and :student not member of c.students "
            + "and c.subject not in (" + "   select g.course.subject "
            + "from Grade g "
            + "where g.student = :student and g.mark.mark <> 5" + ") "
            + "and c not in :#{#student.dismissedCourses}")
    List<Lecture> findAllRecommendableForStudent(
            @Param("student") StudentEntity student);

    @Query("select c "
            + "from Lecture c "
            + "where ?1 member of c.students")
    List<Lecture> findAllForStudent(StudentEntity student);

    @Query("select case when count(c) > 0 then true else false end "
            + "from Lecture c "
            + "where c = ?2 and ?1 member of c.students"
    )
    boolean existsCourseRegistration(StudentEntity student,
                                     Lecture course);

    List<Lecture> findAllBySemester(Semester entity);
}
