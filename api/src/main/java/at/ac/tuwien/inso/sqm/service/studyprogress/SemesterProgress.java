package at.ac.tuwien.inso.sqm.service.studyprogress;

import at.ac.tuwien.inso.sqm.dto.SemesterDto;

import java.util.List;

public class SemesterProgress {

    private SemesterDto semester;

    private List<CourseRegistration> courseRegistrations;

    public SemesterProgress(SemesterDto semester,
                            List<CourseRegistration> courseRegistrations) {
        this.semester = semester;
        this.courseRegistrations = courseRegistrations;
    }

    public SemesterDto getSemester() {
        return semester;
    }

    public List<CourseRegistration> getCourseRegistrations() {
        return courseRegistrations;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SemesterProgress that = (SemesterProgress) o;

        if (getSemester() != null ? !getSemester().equals(that.getSemester())
                : that.getSemester() != null) {
            return false;
        }
        return getCourseRegistrations() != null ? getCourseRegistrations()
                .equals(that.getCourseRegistrations())
                : that.getCourseRegistrations() == null;

    }

    public int hashCode() {
        int result = getSemester() != null ? getSemester().hashCode() : 0;
        result = 31 * result + (getCourseRegistrations() != null
                ? getCourseRegistrations().hashCode() : 0);
        return result;
    }

    public String toString() {
        return "SemesterProgress{"
                + "semester=" + semester
                + ", courseRegistrations="
                + courseRegistrations
                + '}';
    }
}
