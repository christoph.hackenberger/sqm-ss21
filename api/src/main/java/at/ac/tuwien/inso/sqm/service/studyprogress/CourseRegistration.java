package at.ac.tuwien.inso.sqm.service.studyprogress; //FIXME package naming convention?!

import at.ac.tuwien.inso.sqm.entity.Grade;
import at.ac.tuwien.inso.sqm.entity.Lecture;

public class CourseRegistration {

    private Lecture course;

    private CourseRegistrationState state;

    private Grade grade;

    public CourseRegistration(Lecture course) {
        this(course, CourseRegistrationState.IN_PROGRESS);
    }

    public CourseRegistration(Lecture course,
                              CourseRegistrationState state) {
        this.course = course;
        this.state = state;
    }

    public CourseRegistration(Lecture course,
                              CourseRegistrationState state, Grade grade) {
        this.course = course;
        this.state = state;
        this.grade = grade;
    }

    public Lecture getCourse() {
        return course;
    }

    public CourseRegistrationState getState() {
        return state;
    }

    public Grade getGrade() {
        return grade;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CourseRegistration that = (CourseRegistration) o;

        if (getCourse() != null ? !getCourse().equals(that.getCourse())
                : that.getCourse() != null) {
            return false;
        }
        return getState() == that.getState();

    }

    public int hashCode() {
        int result = getCourse() != null ? getCourse().hashCode() : 0;
        result = 31 * result + (getState() != null ? getState().hashCode() : 0);
        return result;
    }

    public String toString() {
        return "CuorseRegistration{course=" + course + ", state=" + state + '}';
    }
}
