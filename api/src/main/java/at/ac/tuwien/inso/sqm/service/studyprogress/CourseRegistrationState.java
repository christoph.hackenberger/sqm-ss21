package at.ac.tuwien.inso.sqm.service.studyprogress;

public enum CourseRegistrationState {
    IN_PROGRESS, NEEDS_FEEDBACK, NEEDS_GRADE, COMPLETE_OK, COMPLETE_NOT_OK
}
