# University Information System #
[![pipeline status](https://gitlab.com/christoph.hackenberger/sqm-ss21/badges/master/pipeline.svg)](https://gitlab.com/christoph.hackenberger/sqm-ss21/-/pipelines)
[![coverage report](https://gitlab.com/christoph.hackenberger/sqm-ss21/badges/master/coverage.svg)](http://christoph.hackenberger.gitlab.io/sqm-ss21/index.html)


The University Information System (UIS) is a web-based management software for universities. <br/>
Students and faculty staff can organize and administrate their courses, the system furthermore assists students by suggesting future courses in a smart way.

## CI Setup ##
The below steps were conducted on an AWS EC2 instance which is also used as a GitLab runner. Note that this instance is not running 24/7, we start/shutdown this instance whenever needed.
The repository where the runner and GitLab pages integration is configured can be found here: https://gitlab.com/christoph.hackenberger/sqm-ss21

The badges on top of this README are clickable and link to the pipeline overview/coverage report deployed on GitLab pages.

### GitLab Runner

In order for the GitLab CI to be able to execute jobs it needs a GitLab Runner. To set up such a runner instance we used docker to deploy it on a small AWS EC2 instance.

We used a [docker-compose file](./docker-compose_gitlabrunner.yml) to create the container in which the GitLab Runner is running.

To start the container we need to run the following command:

```
docker-compose up -f docker-compose_gitlabrunner.yml -d
```

Next we need to configure the runner to connect to the GitLab repository, with the following command:

```
docker-compose run sqm-runner register
```

It will then ask for the url of the GitLab instance and the token of the repository. For the executor we used the _docker_ executor and for the default docker image we selected the latest _alpine_ release.

### Sonarqube

To set up Sonarqube a [docker-compose file](./docker-compose_sonarqube.yml) was used.

Increase Virtual memory limits of machine where Docker is installed (needed for Elastic Search which is used by Sonarqube): `sysctl -w vm.max_map_count=262144`.  
To set this permanently, update the `vm.max_map_count` setting in `/etc/sysctl.conf`.

Execute:
```
docker-compose up -f docker-compose_sonarqube.yml -d
```

Access Sonarqube in web browser (`http://<ip>:9000/`) and add a new project. Set GitLab variables (Settings -> CI/CD -> Variables) for received `projectKey`, `host.url` and `login` (`SONAR_PROJECT`, `SONAR_URL`, `SONAR_LOGIN`). It is recommended to define the `SONAR_LOGIN` variable as "masked" so that the value doesn't show up in the build log.  
The Jacoco path option inside Sonarqube should be set to `../coverage/jacoco.xml` so that the coverage is shown in Sonarqube.

Sonarqube is now ready to use (see `lint` stage in [`.gitlab-ci.yml`](./.gitlab-ci.yml)).

## Setup and running ##

Execute the steps described below on your command-line.

Step 1 has only be done once (as long as you do not delete the files).

### 1. Setup ###

Download Javascript Libraries:

```
cd application/src/main/resources/static
npm install
./node_modules/.bin/webpack
```

_Prerequisite:_ npm (Node.js) has to be available on your computer.

### 2. Build ###

Build your project using Maven:

```
mvn clean install
```
or (in case you want to skip test execution)
```
mvn clean install -DskipTests
```

_Prerequisite:_  Maven 3.3.x or higher and Java 1.8 has to be available on your computer.

### 3. Execution ###

Deploy the application on Spring Boot's Embedded Apache Tomcat 7 Application Server:

```
cd application
mvn spring-boot:run
```

After start-up, the application is available under:
http://localhost:8080/

### 4. Sample Data and User Accounts ###

```at.ac.tuwien.inso.sqm.initializer.DataInitializer``` automatically generates sample data for the application.<br/>
Log in with the following credentials:

```
Student: emma - pass
Lecturer: eric - pass
Administrator: admin - pass
```



