package at.ac.tuwien.inso.sqm.controller;

public final class Constants {

    public static final int MAX_PAGE_SIZE = 10;

    private Constants() {
    }
}
